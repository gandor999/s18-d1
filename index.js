console.log("Hello");

/*
	Objects:

		Syntax:

			let/const objectName = {
				keyA: valueA,
				keyB: valueB
			}
	
*/


let cellphone = {
	name: 'Nokia 3210',
	manufactureDate: 1999
}

console.log("Using initializers/literal notation");
console.log(cellphone);
console.log(typeof cellphone);
console.log(cellphone.name);


/*
	Creating objects using constructor function


		- creates a reusable function to create several objects that have the same data struture

			Syntax:

				function objectName(keyA, keyB){
					this.keyA = keyA;
					this.keyB = keyB;
				}


		- "this" keyword allows to assign a new object's properties by associating them with values recieved from a constructors function's parameters

*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

let laptop = new Laptop('Lenovo', 2008);
console.log('\n\nUsing constructors')
console.log(laptop);

let mylaptop = new Laptop('MacBook Air', 2020);
console.log('\n\nUsing constructors')
console.log(mylaptop);


let mylaptop1 = new Laptop('Acer', 2019);
console.log('\n\nUsing constructors')
console.log(mylaptop1);


// Accessing Object Properties
// dot notation

console.log("\n\nAccessing Object Properties");

console.log("\n\nDot notation: " + mylaptop.name);


// Square bracket notation

console.log("\n\nSqaure bracket notation: " + mylaptop['name']);

let arr = [laptop, mylaptop];

console.log("\n\nArray");

console.log(arr[0]['name']);
console.log(arr[0].name);


// Initializing/Adding/Deleting/Reassigning Object Properties

console.log("\n\nInitializing/Adding/Deleting/Reassigning Object Properties");

let car = {};

car.name = "Honda Civic";

console.log('Result from adding properties using dot notation:');
console.log(car);

car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car);


// Deleting Object Properties
console.log("\n\nDeleting Object Properties");

delete car['manufacture date'];
console.log(car);

// Reassigning Object Properties
console.log("\n\nReassignning Object Properties");

car.name = "E46 BMW M3 GTR";


// Object Method
console.log("\n\nObject Method");

// Method is a function which is a property of an object

let name = "Geodor";

// Without the this keyword name will execute the global variable name = "Geodor" instead

let person = {
	name: 'John',
	talk: function(){
		console.log("Hello my name is " + this.name);
	}
}

console.log(person);
person.talk();


// Adding Methods to Objects
console.log("\n\nAdding Methods to Objects");

person.walk = function(){
	console.log(this.name + ' walked 25 steps forward');
}

person.walk();

// Add a run method to the person object

person.run = function(){
	console.log(this.name + ' ran 100 miles in two days');
}

person.run();

// Creating Reusable Functions

console.log("\n\nCreating Reusable Functions");

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		country: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@email.xyz'],
	introduce: function(){
		console.log('Hello my name is ' + this.firstName + ' ' + this.lastName);
	}
}


friend.introduce();
console.log(friend.address.city);
console.log(friend.address);
console.log(friend.emails);
console.log(friend.emails[1]);
